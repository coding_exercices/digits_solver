# frozen_string_literal: true

require_relative 'digits_solver/version'
require_relative 'digits_solver/error'
require_relative 'digits_solver/problem_statement'
require_relative 'digits_solver/strategies/base'
require_relative 'digits_solver/strategies/brute_force'
require_relative 'digits_solver/solution'
require_relative 'digits_solver/solution_set'

module DigitsSolver
  class Error < StandardError; end

  class DummyLogger
    private

    def respond_to_missing?(method_name, *_)
      return true if %i[debug info warn error fatal].include? method_name

      super
    end

    def method_missing(method_name, *args)
      return if %i[debug info warn error fatal].include? method_name

      super
    end
  end

  # Your code goes here...

  def self.solve_for(target_number, *draw)
    problem_statement = DigitsSolver::ProblemStatement.new target_number, *draw
    DigitsSolver::SolutionSet.solve_for problem_statement
  end

  def self.logger
    @logger ||= DigitsSolver::DummyLogger.new
  end

  def self.logger=(logger)
    @logger = logger
  end
end
