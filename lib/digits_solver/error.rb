# frozen_string_literal: true

module DigitsSolver
  class Error < StandardError; end
end
