# frozen_string_literal: true

module DigitsSolver
  class ProblemStatement
    attr_reader :target_number

    def draw
      @draw.dup
    end

    # noinspection RubyParameterNamingConvention
    def initialize(target_number_or_problem_statement, *draw)
      @target_number, @draw = if target_number_or_problem_statement.is_a? DigitsSolver::ProblemStatement
                                [target_number_or_problem_statement.target_number, target_number_or_problem_statement.draw]
                              else
                                [target_number_or_problem_statement, draw]
                              end
      DigitsSolver.logger.info "The target is #{target_number}"
      DigitsSolver.logger.info "The draw is #{draw.inspect}"
    end

    def max_operations_number
      @max_operations_number ||= @draw.size - 1
    end
  end
end
