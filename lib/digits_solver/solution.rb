# frozen_string_literal: true

module DigitsSolver

  # This is the class that represents a solution to the problem.
  class Solution
    include DigitsSolver::Strategies::Base

    attr_reader :problem_statement, :operands, :operations_to_apply

    def initialize(problem_statement, operands, operations_to_apply)
      raise DigitsSolver::Error, 'Invalid problem statement' unless problem_statement.is_a? DigitsSolver::ProblemStatement
      unless operands.size == operations_to_apply.size + 1
        raise DigitsSolver::Error, "Invalid solution #{operands.inspect} => #{operations_to_apply.inspect}"
      end

      @problem_statement = problem_statement
      @operands = operands
      @operations_to_apply = operations_to_apply
    end

    def ==(other)
      (operations_to_apply == other.operations_to_apply) && (operands == other.operands)
    end

    def to_s
      res = ["Solved in #{operations_to_apply.size} operation#{operations_to_apply.size <= 1 ? "" : "s"}:"]
      res.concat to_operation_lines
      res.join "\n"
    end

    def to_operation_lines
      res = []
      operations_to_apply.each.with_index.reduce(operands.first) do |acc, (operation, idx)|
        computed_result = apply_operation_to_operands operation, acc, operands[idx + 1]
        res << format(' => %u %s %u = %u',
                      acc,
                      DigitsSolver::Strategies::Base::OPERATIONS[operation],
                      operands[idx + 1],
                      computed_result)
        computed_result
      end
      res
    end

    def pretty_print(pp)
      pp.object_address_group(self) do
        pp.breakable
        pp.text "Solution: '#{to_evaluable_code} = #{problem_statement.target_number}'"
        pp.text ','
        pp.breakable
        pp.seplist(instance_variables) do |v|
          pp.text "#{v}="
          pp.pp instance_variable_get v
        end
      end
    end

    def to_evaluable_code
      # parenthesis management is ... meh, but better than nothing, and at least mathematically correct.
      evaluable_code = operations_to_apply.each.with_index.reduce(operands.first) do |res, (operation, idx)|
        op1 = res
        op2 = operands[idx + 1]
        format_string = operation == :multiply ? '%s %s %u' : '(%s %s %u)'
        format(format_string, op1, DigitsSolver::Strategies::Base::OPERATIONS[operation], op2)
      end
      evaluable_code[-1] == ')' ? evaluable_code[1...-1] : evaluable_code
    end
  end
end
