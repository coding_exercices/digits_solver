# frozen_string_literal: true

module DigitsSolver
  class SolutionSet
    include Enumerable

    DEFAULT_STRATEGY = DigitsSolver::Strategies::BruteForce
    attr_reader :problem_statement, :strategy, :count

    class << self
      def solve_for(problem_statement, strategy: DEFAULT_STRATEGY)
        extend strategy
        solutions, attempts_count = solve(problem_statement)
        DigitsSolver.logger.info "Found #{solutions.count} potentially non unique solutions amongst #{attempts_count} tries."
        new problem_statement, solutions, strategy
      end
    end

    def each(&block)
      solutions.each(&block)
    end

    def sorted_solutions
      indexed_solutions.keys
                       .sort { |a, b| a.size <=> b.size }
                       .map { |k| indexed_solutions[k] }
                       .flatten
    end

    def best_solution(nb = 1)
      res = indexed_solutions.keys
                             .sort { |a, b| a.size <=> b.size }
                             .take(nb)
                             .map { |k| indexed_solutions[k] }
                             .flatten
                             .take(nb)
      nb == 1 ? res.first : res
    end

    alias best_solutions best_solution

    def size
      solutions.size
    end

    private

    attr_reader :solutions, :indexed_solutions

    def <<(solution)
      return nil if solutions.include? solution

      solutions << solution
      indexed_solutions[solution.operands] ||= []
      indexed_solutions[solution.operands] << solution
      solutions
    end

    def initialize(problem_statement, solutions, strategy)
      raise DigitsSolver::Error, 'Invalid problem statement' unless problem_statement.is_a? DigitsSolver::ProblemStatement

      @problem_statement = problem_statement
      @strategy = strategy

      @indexed_solutions = {}
      @solutions = []

      DigitsSolver.logger.info "#{solutions.count} candidate solutions"
      discarded_nb = solutions.reduce(0) do |count, solution|
        if self << solution
          DigitsSolver.logger.debug "New solution added to solution set: #{solution.to_evaluable_code}"
          count
        else
          count + 1
        end
      end
      DigitsSolver.logger.info "Discarded: #{discarded_nb}"
      msg = "Added #{self.solutions.count} solutions to the set of solutions, "
      msg += discarded_nb.zero? ? 'none' : discarded_nb.to_s
      msg += ' were discarded'
      DigitsSolver.logger.info msg
    end
  end
end
