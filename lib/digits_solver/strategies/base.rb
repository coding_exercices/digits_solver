# frozen_string_literal: true

module DigitsSolver
  module Strategies

    module Base
      class OperationError < StandardError; end

      OPERATIONS = {
        plus: :+,
        minus: :-,
        multiply: :*,
        divide: :/
      }.freeze

      def solve(_)
        raise DigitsSolver::Error 'You should not be there !'
      end

      protected

      # noinspection RubyInstanceMethodNamingConvention,RubyParameterNamingConvention
      def possible_operations_for_ordered_draw(max_operations_number)
        nb_operations = OPERATIONS.size
        ops_a = OPERATIONS.to_a
        number_of_possibilities = nb_operations**max_operations_number

        DigitsSolver.logger.info "The number of operations permutations for the #{nb_operations} basic operations and a draw of #{max_operations_number + 1} numbers (ie #{max_operations_number} consecutive operations) is #{number_of_possibilities}."

        (0...number_of_possibilities).map do |i|
          i.to_s(nb_operations)
           .ljust(max_operations_number, '0')
           .chars
           .map { |operation_index_str| ops_a[operation_index_str.to_i].first }
        end
      end

      # noinspection RubyInstanceMethodNamingConvention
      def apply_operations_chain_to_ordered_draw(operations_chain, ordered_draw, problem_statement)
        operations_chain.each.with_index.reduce(ordered_draw.first) do |acc, (operation, idx)|
          cur_op_res = apply_operation_to_operands(operation, acc, ordered_draw[idx + 1])
          if cur_op_res == problem_statement.target_number
            ops = operations_chain.take(idx + 1)
            operands = ordered_draw.take(idx + 2)
            yield DigitsSolver::Solution.new(problem_statement, operands, ops)
            return cur_op_res

          end
          cur_op_res
        end
      end

      def apply_operation_to_operands(operation, operand1, operand2)
        # wow that's a useful one ;-), at least more understandable
        send operation, operand1, operand2
      end

      def plus(a, b)
        a + b
      end

      def minus(a, b)
        raise DigitsSolver::Strategies::Base::OperationError, "#{a} - #{b} is not allowed as result would be negative" unless a >= b

        a - b
      end

      def multiply(a, b)
        a * b
      end

      def divide(a, b)
        unless (a % b).zero?
          raise DigitsSolver::Strategies::Base::OperationError,
                "#{a} / #{b} is not allowed as result would not be an integer"
        end

        a / b
      end
    end
  end
end
