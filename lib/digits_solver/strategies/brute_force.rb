# frozen_string_literal: true

module DigitsSolver
  module Strategies
    module BruteForce
      include DigitsSolver::Strategies::Base

      def solve(problem_statement)
        solutions = []
        draw = problem_statement.draw
        possible_operations = possible_operations_for_ordered_draw problem_statement.max_operations_number
        # DigitsSolver.logger.debug possible_operations.inspect
        attempts_count = 0

        draw.permutation.map do |ordered_draw|
          DigitsSolver.logger.debug "Testing #{ordered_draw.inspect} for all possible chains of operations"
          possible_operations.each do |operations_chain|
            attempts_count += 1
            begin
              apply_operations_chain_to_ordered_draw(operations_chain, ordered_draw, problem_statement) do |valid_solution|
                solutions << valid_solution
              end
            rescue DigitsSolver::Strategies::Base::OperationError => e
              DigitsSolver.logger.debug "#{ordered_draw.inspect} => #{operations_chain.inspect} is discarded because #{e.message}"
            end
          end
        end
        DigitsSolver.logger.debug "Found #{solutions.count} solutions."
        [solutions, attempts_count]
      end
    end
  end
end
