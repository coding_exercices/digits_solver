# frozen_string_literal: true

RSpec.describe DigitsSolver do
  subject { described_class }

  let(:draws) do
    [
      { target: 10, draw: [1, 2, 3, 4, 5] },
      { target: 477, draw: [3, 5, 9, 13, 19, 25] }
    ]
  end

  it 'has a version number' do
    expect(subject::VERSION).not_to be nil
  end

  it 'should find solutions' do
    draws.each do |draw_hash|
      r = nil
      expect { r = subject.solve_for draw_hash[:target], *draw_hash[:draw] }.not_to raise_error
      expect(r).to be_a DigitsSolver::SolutionSet
      # noinspection RubyNilAnalysis
      expect(r.sorted_solutions).not_to be_empty
      expect(r.best_solution).not_to be_nil
      expect(r.best_solution).to be_a DigitsSolver::Solution
      expect(r.best_solutions(2)).to be_an Array
      DigitsSolver.logger.info r.best_solution.to_evaluable_code
    end
  end

  it 'should only return valid solutions' do
    draws.each do |draw_hash|
      r = nil
      expect { r = subject.solve_for draw_hash[:target], *draw_hash[:draw] }.not_to raise_error
      DigitsSolver.logger.info "The target number for draw #{r.problem_statement.draw.inspect} is #{r.problem_statement.target_number}."
      r.sorted_solutions.each do |solution|
        expect { eval(solution.to_evaluable_code) }.not_to raise_error
        expect(eval(solution.to_evaluable_code)).to eq r.problem_statement.target_number
      end
      DigitsSolver.logger.info "All #{r.size} solutions found match."
    end
  end

  context 'when the solution is not reachable' do
    let(:draws) do
      [
        { target: 1000, draw: [1, 2, 3, 4, 5] },
        { target: 455_577, draw: [3, 5, 9, 13, 25] }
      ]
    end

    it 'should not fail just return an empty SolutionSet' do
      draws.each do |draw_hash|
        r = nil
        expect { r = subject.solve_for draw_hash[:target], *draw_hash[:draw] }.not_to raise_error
        expect(r).to be_a DigitsSolver::SolutionSet
        # noinspection RubyNilAnalysis
        expect(r.size).to eq 0
        expect(r.sorted_solutions).to be_empty
        expect(r.best_solution).to be_nil
        expect(r.best_solutions(2)).to be_an Array
        expect(r.best_solutions(2)).to be_empty
      end
    end
  end
end
