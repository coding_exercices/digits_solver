# frozen_string_literal: true

require 'digits_solver'

# If you want logs activated during tests use the 'DEBUG_DIGITS_SOLVER' environment variable
unless ENV['DEBUG_DIGITS_SOLVER'].nil?
  require 'logger'
  DigitsSolver.logger = Logger.new($stderr)
  # Set to 0 if you want verbooooose output
  DigitsSolver.logger.level = 1
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
